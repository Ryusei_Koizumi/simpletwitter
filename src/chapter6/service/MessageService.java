package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String userId){
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();

			//idを初期化
			Integer id = null;
			//userIdが空じゃない場合、userIdの値をint型のidへ保管する
			if(!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}
			//Daoから返されたmessagesを保管する
			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM);
			//DBに情報をコミットする
			commit(connection);

			//messagesを返す
			return messages;
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}
}
